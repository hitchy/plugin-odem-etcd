# plugin-odem-etcd [![pipeline status](https://gitlab.com/hitchy/plugin-odem-etcd/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-odem-etcd/-/commits/master)

connecting [Hitchy](http://hitchy.org)'s [document-oriented database](https://odem.hitchy.org/) with [etcd](https://etcd.io/) cluster

## License

[MIT](LICENSE)

## About

[Hitchy](http://hitchy.org) is a server-side framework for developing web applications with [Node.js](https://nodejs.org). [Odem](https://odem.hitchy.org/) is a plugin for Hitchy implementing a document-oriented database using data backends like regular file systems, temporary in-memory databases and third-party key-value stores. Accessing either backend requires some _adapter_.
 
This package is implementing an adapter for storing data in an [etcd-based cluster](https://etcd.io/).


## Installation

Execute the following command in the folder of your Hitchy-based application to install this adapter:

```bash
npm install @hitchy/plugin-odem-etcd
```

The adapter depends on [@hitchy/plugin-odem](https://www.npmjs.com/package/@hitchy/plugin-odem), which in turn depends on [@hitchy/core](https://www.npmjs.com/package/@hitchy/core). Either dependency must be installed manually as well:

```bash
npm install @hitchy/core @hitchy/plugin-odem
```

## Usage

Select an instance of this backend as default adapter in your application's configuration by creating a file **config/database.js** with content like this:

```javascript
import File from "node:fs";

export const database = {
    default : new this.services.OdemAdapterEtcd( {
        hosts: [
            "https://10.0.1.1:2379",
            "https://10.0.1.2:2379",
            "https://10.0.1.3:2379",
        ],
        retry: false,
        credentials: {
            rootCertificate: File.readFileSync( "path/to/ca.pem" ),
            certChain: File.readFileSync( "path/to/cert.pem" ),
            privateKey: File.readFileSync( "path/to/key.pem" ),
        },
        auth: { username: "john.doe", password: "secret" },
        prefix: "common/prefix/user/can/readwrite",
    } ),
};
```

Most of provided [options](https://mixer.github.io/etcd3/interfaces/options_.ioptions.html) are forwarded to [instance of Etcd3 created internally](https://mixer.github.io/etcd3/classes/namespace_.namespace.html).

* `hosts` is a list of endpoint URLs of etcd cluster to connect with.

  The given example illustrates encrypted connections via https using IP addresses. Depending on your setup using host names may be available, as well.
   
* `retry` is a boolean controlling whether client should retry queries when one of the tested nodes in list is not available or is having temporary issues. 

  This feature is enabled by default and so you do not need to provide it here unless you want to disable it.
  
* `credentials` is an object selecting a client TLS certificate for authenticating with the cluster.

  Using this feature is optional. However, using this might require connection encrypted via https, only.

* `auth` is an object providing authentication data for [role-based access control](https://etcd.io/docs/latest/op-guide/authentication/) (RBAC). It contains properties `username` and `password` containing either information in cleartext.

  It's okay to omit this when connecting with a cluster that does not use RBAC. You should consider RBAC when connecting different applications to a single etcd cluster.

In opposition to those, the following options are consumed by this adapter:

* `prefix` defines a common prefix to use on every read/write operation of current application. When omitted, the prefix defaults to `hitchy-odem`.

  This plugin assumes the prefix to be a Unix-style path name. In this, it is ignoring any trailing forward slash, but keeps leading ones. Based on common practice, the prefix should not have a leading forward slash. Thus, an example complying with this plugin's assumptions would be `my-user/the-app/data`.

  When authenticating against etcd with a username and password, that user may have access to keys sharing a common prefix. In such a case providing at least that shared prefix here is mandatory.
