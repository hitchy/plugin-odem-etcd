import { randomUUID } from "node:crypto";
import { PassThrough } from "node:stream";
import { Etcd3, EtcdLockFailedError } from "etcd3";
import { ExponentialBackoff, handleType, retry } from "cockatiel";

/**
 * @typedef {object} IOptions see https://mixer.github.io/etcd3/interfaces/options_.ioptions.html
 */

/**
 * @typedef {IOptions} EtcdAdapterOptions
 * @property {string} prefix prefix to use on reading/writing keys from/in etcd cluster
 */

const DefaultOptions = {
	prefix: "hitchy-odem",
};

/** */
export default function() {
	const api = this;

	const logDebug = api.log( "hitchy:odem:etcd:debug" );
	const logError = api.log( "hitchy:odem:etcd:error" );

	/**
	 * Implements adapter for saving odem models in an etcd cluster.
	 */
	class OdemAdapterEtcd extends api.service.OdemAdapter {
		/**
		 * @param {EtcdAdapterOptions} options options selecting cluster to use
		 */
		constructor( options = {} ) {
			super();

			const _options = Object.assign( {}, DefaultOptions, options );
			const prefix = _options.prefix = ( String( _options.prefix || "" ).trim().replace( /\/+$/, "" ) + "/" ).replace( /^\/$/, "" );

			const client = new Etcd3( _options );

			this.maxAttempts = 10;

			Object.defineProperties( this, {
				/**
				 * Exposes client connecting with etcd cluster.
				 *
				 * @name OdemAdapterEtcd#client
				 * @property {Etcd3}
				 * @readonly
				 */
				client: { value: prefix === "" ? client : client.namespace( prefix ) },

				/**
				 * Exposes prefix to use for mapping internal keys into those
				 * used in connected etcd cluster.
				 *
				 * @name OdemAdapterEtcd#prefix
				 * @property {string}
				 * @readonly
				 */
				prefix: { value: prefix },

				/**
				 * Exposes options eventually used for customizing adapter.
				 *
				 * @name OdemAdapterEtcd#options
				 * @property {object}
				 * @readonly
				 */
				options: {
					get: () => {
						const copy = {};
						const names = Object.keys( _options );
						const numNames = names.length;

						for ( let i = 0; i < numNames; i++ ) {
							const name = names[i];

							if ( name === "credentials" || name === "auth" ) {
								copy[name] = "provided, but hidden";
							} else {
								copy[name] = _options[name];
							}
						}

						Object.defineProperty( this, "options", { value: copy } );

						return copy;
					},
					configurable: true,
				},
			} );

			this.client.watch()
				.prefix( "" )
				.withPreviousKV()
				.create()
				.then( watcher => {
					logDebug( "setting up watcher for remote changes at %j", this.options.hosts );

					api.once( "shutdown", () => {
						logDebug( "shutting down watcher for remote changes at %j", this.options.hosts );

						watcher.cancel();
					} );

					watcher
						.on( "error", error => {
							logError( "etcd error: %s", error.message );
						} )
						.on( "disconnected", () => {
							logDebug( "disconnected from cluster" );
						} )
						.on( "connected", () => {
							logDebug( "connected with cluster" );
						} )
						.on( "put", ( now, before ) => {
							const key = now.key.toString( "utf8" );

							if ( now.value.length < 1 ) {
								logDebug( "ignoring change notification on %s without data", key );
								return;
							}

							const raw = now.value.toString( "utf8" );
							let value, beforeValue;

							try {
								value = JSON.parse( raw );
							} catch ( error ) {
								logError( "ignoring change notification on %s with invalid data: %s (%j)", key, error.message, raw );
								return;
							}

							if ( before ) {
								try {
									beforeValue = JSON.parse( before.value.toString( "utf8" ) );
								} catch ( error ) {
									logError( "omitting invalid previous data in change notification on %s: %s (%j)", key, error.message, before.value );
								}
							}

							logDebug( "got remote change notification on %s", key );

							this.emit( "change", key, value, before ? {
								key: before.key,
								value: beforeValue,
							} : undefined );
						} )
						.on( "delete", res => {
							const key = res.key.toString( "utf8" );

							logDebug( "got remote removal notification on %s", key );

							this.emit( "delete", key );
						} );
				} )
				.catch( error => {
					logError( "FATAL: setting up watcher for cluster-side changes of data failed: %s", error.stack );
				} );
		}

		/**
		 * Drops all data available via current adapter.
		 *
		 * @note This method is primarily meant for use while testing. It might be
		 *       useful in similar situations as well, like uninstalling some app.
		 *
		 * @returns {Promise} promises purging all data available via current adapter
		 */
		purge() {
			return this.client.delete().all().then( () => undefined );
		}

		/**
		 * Puts provided data in storage assigning new unique key.
		 *
		 * @param {string} keyTemplate template of key containing %u to be replaced with assigned UUID
		 * @param {object} data record to be written
		 * @returns {Promise.<string>} promises unique key of new record
		 */
		async create( keyTemplate, data ) {
			const that = this;

			logDebug( "creating entry at template %s and %j", keyTemplate, data );

			return await retry( handleType( EtcdLockFailedError ), {
				maxAttempts: this.maxAttempts || 10,
				backoff: new ExponentialBackoff(),
			} ).execute( () => this.client.lock( keyTemplate.replace( /%u.*$/, "" ) )
				.do( () => new Promise( ( resolve, reject ) => {
					_create( 0, 100 );

					/**
					 * Implements single attempt for writing key with random
					 * UUID without overwriting some existing one.
					 *
					 * @param {int} current index of current iteration
					 * @param {int} stopAt max. number of iterations before failing
					 * @returns {void}
					 */
					function _create( current, stopAt ) {
						if ( current >= stopAt ) {
							reject( new Error( "could not find available UUID after reasonable number of attempts" ) );
						} else {
							const key = keyTemplate.replace( /%u/g, randomUUID() );

							that.client.get( key )
								.then( value => {
									if ( value == null ) {
										logDebug( "creating entry at %s containing %j", key, data );

										return that.client.put( key )
											.value( JSON.stringify( data ) )
											.then( () => resolve( key ) );
									}

									process.nextTick( _create, current + 1, stopAt );

									return undefined;
								} )
								.catch( reject );
						}
					}
				} ) ) );
		}

		/**
		 * Checks if provided key exists.
		 *
		 * @param {string} key unique key of record to test
		 * @returns {Promise.<boolean>} promises information if key exists or not
		 */
		async has( key ) {
			try {
				return Boolean( await this.client.get( key ) );
			} catch ( cause ) {
				logError( "testing key %s caused error: %s", key, cause.stack );

				throw cause;
			}
		}

		/**
		 * Reads data selected by provided key.
		 *
		 * @param {string} key unique key of record to read
		 * @param {object} ifMissing data object to return if selected record is missing
		 * @returns {Promise.<object>} promises read data
		 */
		async read( key, { ifMissing = null } = {} ) {
			logDebug( "fetching entry at %s", key );

			const value = await this.client.get( key );

			return value == null ? ifMissing : JSON.parse( value );
		}

		/**
		 * Writes provided data to given key.
		 *
		 * @note To support REST API in hitchy-plugin-odem-rest this method must be
		 *       capable of writing to record that did not exist before, thus
		 *       creating new record with provided key.
		 *
		 * @param {string} key unique key of record to be written
		 * @param {object} data record to be written
		 * @returns {Promise<object>} promises provided data
		 */
		async write( key, data ) {
			logDebug( "updating entry at %s with %j", key, data );

			const value = JSON.stringify( data );

			await retry( handleType( EtcdLockFailedError ), {
				maxAttempts: this.maxAttempts || 10,
				backoff: new ExponentialBackoff(),
			} ).execute( () => this.client.put( key ).value( value ).exec() );

			return data;
		}

		/**
		 * Removes data addressed by given key.
		 *
		 * @note Removing some parent key includes removing all subordinated keys.
		 *
		 * @param {string} key unique key of record to be removed
		 * @returns {Promise<string>} promises key of removed data
		 */
		async remove( key ) {
			logDebug( "removing entry at %s", key );

			await retry( handleType( EtcdLockFailedError ), {
				maxAttempts: this.maxAttempts || 10,
				backoff: new ExponentialBackoff(),
			} ).execute( () => this.client.delete().key( key ).exec() );

			return key;
		}

		/**
		 * Retrieves stream of available keys.
		 *
		 * @param {string} prefix stream keys with given prefix, only
		 * @param {int} maxDepth skip keys beyond this depth (relative to `prefix`)
		 * @param {string} separator consider this character separating segments of key selecting different depth, set null to disable depth processing
		 * @param {"skip"|"fail"} invalidPolicy selects policy for handling invalid records, e.g. failing to contain valid JSON
		 * @returns {Readable} stream of keys
		 */
		keyStream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			invalidPolicy = "skip",
		} = {} ) {
			return this.stream( { prefix, maxDepth, separator, invalidPolicy, target: "key" } );
		}

		/**
		 * Retrieves stream of available keys, values or key-value-pairs.
		 *
		 * @param {string} prefix stream pairs with keys matching this prefix, only
		 * @param {int} maxDepth skip keys beyond this depth (relative to `prefix`)
		 * @param {string} separator consider this character separating segments of key selecting different depth, set null to disable depth processing
		 * @param {"entry"|"key"|"value"} target selects data to be provided per key-value pair
		 * @param {"skip"|"fail"} invalidPolicy selects policy for handling invalid records, e.g. failing to contain valid JSON
		 * @returns {Readable} stream of keys, values or key-value pairs
		 */
		stream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			target = "entry",
			invalidPolicy = "skip",
		} = {} ) {
			const stream = new PassThrough( { objectMode: true } );
			const sep = separator || "/";
			let _prefix = String( prefix ?? "" ).trim();

			while ( _prefix.endsWith( sep ) ) {
				_prefix = _prefix.slice( 0, _prefix.length - 1 );
			}

			const ns = _prefix === "" ? this.client : this.client.namespace( _prefix + sep );

			logDebug( "streaming key-value pairs", _prefix === "" ? "without prefix" : "with prefix " + _prefix );

			ns.getAll().then( pairs => {
				const keys = Object.keys( pairs || {} );
				const numKeys = keys.length;
				const collected = new Array( numKeys );
				let written = 0;

				logDebug( "got %d raw etcd-side key-value pair(s)", numKeys );

				// limit either key to certain maximum depth and collect it in a map
				for ( let read = 0; read < numKeys; read++ ) {
					let key = keys[read];
					let value = pairs[key];

					if ( separator != null && maxDepth < Infinity ) {
						const segments = key.split( sep );
						if ( segments > maxDepth ) {
							continue;
						}
					}

					key = _prefix === "" ? key : _prefix + sep + key;

					try {
						value = JSON.parse( value );
					} catch ( cause ) {
						switch ( invalidPolicy ) {
							case "fail" :
								logError( "parsing etcd record at %s as JSON failed: %s", key, cause.message );
								throw cause;

							case "skip" :
								logDebug( "skipping invalid/malformed etcd entry at %s: %s", key, cause.message );
								continue;

							default :
								throw new TypeError( "unsupported invalidPolicy parameter" );
						}
					}

					let item;

					switch ( target ) {
						case "entry" :
							item = { key, value };
							break;

						case "key" :
							item = key;
							break;

						case "value" :
							item = value;
							break;

						default :
							throw new TypeError( "invalid target parameter" );
					}

					collected[written++] = item;
				}

				collected.splice( written );

				logDebug( "got %d unique Odem-related record(s)", collected.length );

				_write( 0, collected );

				/**
				 * Pushes items into writable stream pausing whenever
				 * hitting the stream's highWaterMark.
				 *
				 * @param {int} cursor index of next item to write
				 * @param {Array} items set of items to be written
				 * @returns {void}
				 */
				function _write( cursor, items ) {
					const numItems = items.length;

					for ( let i = cursor; i < numItems; i++ ) {
						if ( !stream.write( items[i] ) ) {
							stream.once( "drain", () => _write( i + 1, items ) );
							return;
						}
					}

					stream.end();
				}
			} )
				.catch( error => stream.destroy( error ) );

			return stream;
		}

		/**
		 * Indicates if adapter is capable of storing Buffer as property value.
		 *
		 * This information can be used by property type handlers on serializing
		 * data for storing via this adapter.
		 *
		 * @returns {boolean} true if adapter can save binary buffers as property value
		 */
		static get supportsBinary() { return false; }
	}

	return OdemAdapterEtcd;
};
