import { randomInt } from "node:crypto";
import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";

const Test = await SDT( Core );

const delay = ms => new Promise( resolve => setTimeout( resolve, ms ) );

const config = nodes => ( {
	projectFolder: "../project",
	plugin: true,
	options: {
		// debug: true,
		arguments: {
			nodes,
		},
	},
} );

const letters = "abcdefghijklmnopqrstuvwxyz";
const getRandomName = async length => {
	let string = "";

	while ( string.length < length ) {
		const index = await new Promise( resolve => randomInt( 0, letters.length - 1, ( _, value ) => resolve( value ) ) ); // eslint-disable-line no-await-in-loop
		string += letters[index];
	}

	return string;
};

const myCounter = await getRandomName( 16 );

describe( "etcd adapter", function() {
	this.timeout( 30000 ); // high timeout becomes necessary when running tests in parallel

	const ctx1 = {};
	const ctx2 = {};

	before( Test.before( ctx1, config( "https://etcd-he1.cepharum.de:2379,https://etcd-fs1.cepharum.de:2379" ) ) );
	after( Test.after( ctx1 ) );

	before( Test.before( ctx2, config( "https://etcd-nb1.cepharum.de:2379,https://etcd-nb2.cepharum.de:2379" ) ) );
	after( () => ctx2.delete( `/api/counter/${myCounter}` ) );
	after( Test.after( ctx2 ) );

	it( "establishes authenticated connection with a remote etcd cluster", async() => {
		const res = await ctx1.get( "/api/counter" );

		res.statusCode.should.be.equal( 200 );
		res.data.should.be.an.Array();
	} );

	it( "manages data in connected cluster", async() => {
		const { data: a } = await ctx1.post( "/api/counter/foo/increase" );

		a.should.be.an.Object().which.containDeep( { name: "foo" } );
		a.value.should.be.a.Number().which.is.greaterThan( 0 );

		const { data: b } = await ctx2.post( "/api/counter/foo/increase" );

		b.should.be.an.Object().which.containDeep( { name: "foo" } );
		b.value.should.be.a.Number().which.is.greaterThan( 0 );

		b.value.should.be.greaterThan( a.value );
	} );

	it( "watches cluster for remote change of records", async() => {
		await ctx1.post( `/api/counter/${myCounter}/increase` );

		const [ change, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.post( `/api/counter/${myCounter}/increase` ) ),
			ctx2.get( `/api/watch/${myCounter}/changed` ),
		] );

		change.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		change.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		change.data.name.should.be.ok().and.equal( notification.data.now.name );
		change.data.value.should.be.ok().and.equal( notification.data.now.value );

		change.data.name.should.be.ok().and.equal( notification.data.before.name );
		change.data.value.should.be.ok().and.equal( notification.data.before.value + 1 );
	} );

	it( "watches cluster for remote creation of records", async() => {
		( await ctx1.delete( `/api/counter/${myCounter}` ) ).statusCode.should.be.equal( 200 );

		const [ creation, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.post( `/api/counter/${myCounter}/increase` ) ),
			ctx2.get( `/api/watch/${myCounter}/created` ),
		] );

		creation.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		creation.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		creation.data.name.should.be.ok().and.equal( notification.data.now.name );
		creation.data.value.should.be.ok().and.equal( notification.data.now.value );

		( notification.data.before == null ).should.be.ok();
	} );

	it( "watches cluster for remote removal of records", async() => {
		const record = await ctx1.post( `/api/counter/${myCounter}/increase` );

		record.statusCode.should.be.equal( 200 );

		const [ removal, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.delete( `/api/counter/${myCounter}` ) ),
			ctx2.get( `/api/watch/${record.data.uuid}/removed` ),
		] );

		removal.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		record.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		( notification.data.now == null ).should.be.ok();
		( notification.data.before == null ).should.be.ok();
	} );

	it( "watches cluster for local change of records", async() => {
		await ctx1.post( `/api/counter/${myCounter}/increase` );

		const [ change, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.post( `/api/counter/${myCounter}/increase` ) ),
			ctx1.get( `/api/watch/${myCounter}/changed` ),
		] );

		change.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		change.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		change.data.name.should.be.ok().and.equal( notification.data.now.name );
		change.data.value.should.be.ok().and.equal( notification.data.now.value );

		change.data.name.should.be.ok().and.equal( notification.data.before.name );
		change.data.value.should.be.ok().and.equal( notification.data.before.value + 1 );
	} );

	it( "watches cluster for local creation of records", async() => {
		( await ctx1.delete( `/api/counter/${myCounter}` ) ).statusCode.should.be.equal( 200 );

		const [ creation, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.post( `/api/counter/${myCounter}/increase` ) ),
			ctx1.get( `/api/watch/${myCounter}/created` ),
		] );

		creation.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		creation.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		creation.data.name.should.be.ok().and.equal( notification.data.now.name );
		creation.data.value.should.be.ok().and.equal( notification.data.now.value );

		( notification.data.before == null ).should.be.ok();
	} );

	it( "watches cluster for local removal of records", async() => {
		const record = await ctx1.post( `/api/counter/${myCounter}/increase` );

		record.statusCode.should.be.equal( 200 );

		const [ removal, notification ] = await Promise.all( [
			delay( 100 ).then( () => ctx1.delete( `/api/counter/${myCounter}` ) ),
			ctx1.get( `/api/watch/${record.data.uuid}/removed` ),
		] );

		removal.statusCode.should.be.equal( 200 );
		notification.statusCode.should.be.equal( 200 );

		record.data.uuid.should.be.ok().and.equal( notification.data.uuid );

		( notification.data.now == null ).should.be.ok();
		( notification.data.before == null ).should.be.ok();
	} );
} );
