export default {
	props: {
		name: { required: true, index: "eq" },
		value: { type: "number", default: 0 },
	},
};
