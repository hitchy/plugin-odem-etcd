export default function( options ) {
	const api = this;
	const { services } = api;

	const etcdNodes = String( options.arguments.nodes )
		.split( /[,;\s]+/ )
		.filter( i => i.trim() );

	if ( etcdNodes.length > 0 ) {
		return {
			database: {
				default: new services.OdemAdapterEtcd( {
					hosts: etcdNodes,
					auth: {
						username: "hitchy-plugin-etcd-ci-test",
						password: options.environment.ETCD_PASSWORD,
					},
					prefix: "hitchy-plugin-etcd-ci-test",
				} ),
			},
		};
	}

	throw new Error( "missing etcd configuration" );
};
