export const routes = {
	"GET /api/counter": async( req, res ) => {
		res.json( await req.api.model.Counter.list() );
	},
	"GET /api/counter/:name": async( req, res ) => {
		const counters = await req.api.model.Counter.find( { eq: { name: req.params.name } } );

		if ( counters.length > 0 ) {
			await counters[0].load();

			req.api.log( "etcd-test:info" )( "fetching counter %s with name %s at %d", counters[0].uuid, req.params.name, counters[0].value );

			res.json( counters[0].toObject( { serialized: true } ) );
		} else {
			res.status( 404 ).json( { error: "no such counter" } );
		}
	},
	"DELETE /api/counter/:name": async( req, res ) => {
		const counters = await req.api.model.Counter.find( { eq: { name: req.params.name } } );

		req.api.log( "etcd-test:info" )( "removing counter(s) %s with name %s", counters.map( c => c.uuid ), req.params.name );

		await Promise.all( counters.map( counter => counter.remove() ) );

		res.status( 200 ).json( {} );
	},
	"POST /api/counter/:name/increase": async( req, res ) => {
		const counters = await req.api.model.Counter.find( { eq: { name: req.params.name } } );

		if ( counters.length > 0 ) {
			req.api.log( "etcd-test:info" )( "increasing counter %s with UUID %s at %d", req.params.name, counters[0].uuid, counters[0].value );
			counters[0].value++;
		} else {
			counters[0] = new req.api.model.Counter();
			counters[0].name = req.params.name;
			counters[0].value = 1;
		}

		await counters[0].save();

		res.json( counters[0].toObject( { serialized: true } ) );
	},
	"GET /api/watch/:name/changed": ( req, res ) => {
		return new Promise( ( resolve, reject ) => {
			setTimeout( reject, 5000, new req.api.service.HttpException( 408, "waiting for notification has timed out" ) );

			req.api.model.Counter.notifications.on( "changed", ( uuid, now, before ) => {
				if ( now.name === req.params.name ) {
					resolve( { uuid: String( uuid ), now, before } );
				}
			} );
		} )
			.then( data => res.json( data ) )
			.catch( cause => res.status( cause.statusCode || 500 ).json( { error: cause.message } ) );
	},
	"GET /api/watch/:name/created": ( req, res ) => {
		return new Promise( ( resolve, reject ) => {
			setTimeout( reject, 5000, new req.api.service.HttpException( 408, "waiting for notification has timed out" ) );

			req.api.model.Counter.notifications.on( "created", ( uuid, now ) => {
				if ( now.name === req.params.name ) {
					resolve( { uuid: String( uuid ), now } );
				}
			} );
		} )
			.then( data => res.json( data ) )
			.catch( cause => res.status( cause.statusCode || 500 ).json( { error: cause.message } ) );
	},
	"GET /api/watch/:uuid/removed": ( req, res ) => {
		return new Promise( ( resolve, reject ) => {
			setTimeout( reject, 5000, new req.api.service.HttpException( 408, "waiting for notification has timed out" ) );

			req.api.model.Counter.notifications.on( "removed", uuid => {
				if ( String( uuid ) === req.params.uuid ) {
					resolve( { uuid: String( uuid ) } );
				}
			} );
		} )
			.then( data => res.json( data ) )
			.catch( cause => res.status( cause.statusCode || 500 ).json( { error: cause.message } ) );
	},
};
